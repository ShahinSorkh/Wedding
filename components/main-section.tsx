import bgSrc from "@/app/AV-logo-out.svg";
import imageSrc from "@/app/IMG_0337-Recovered.png";
import Image from "next/image";
import Link from "next/link";
import Script from "next/script";
import { FaLocationDot } from "react-icons/fa6";

type Props = {
  headline: string;
  imageAlt: string;
  slogan: string[];
  description: string[];
  location: { text: string; url: string; btnText: string };
  date: { month: string; day: string; year: string; weekday: string };
  time: { start: string; end: string };
  kicker: string;
};

export default function MainSection({
  headline,
  imageAlt,
  slogan,
  description,
  kicker,
  date,
  time,
  location,
}: Props) {
  return (
    <main>
      <section className="mx-auto my-14 w-full flex-wrap max-w-full flex flex-col justify-evenly sm:flex-row md:px-32">
        <div className="relative px-8 sm:flex-1">
          <Image
            placeholder="blur"
            alt={imageAlt}
            className="overflow-hidden w-full h-full sm:min-w-[364px] max-w-xl"
            src={imageSrc}
          />
          <div className="absolute left-4 top-2/3 px-10 py-4 text-white bg-blue-shahin xs:px-6 xs:py-4 xs:pt-8">
            {slogan.map((s, i) => (
              <p
                className="text-3xl/tight font-dima-shekasteh xs:text-3xl/normal"
                key={`slo-${i}`}
              >
                {s}
              </p>
            ))}
          </div>
        </div>
        <div className="relative flex flex-col justify-between px-8 sm:flex-1 md:mt-0">
          <div dir="rtl" className="mt-10 mb-5">
            <h1 className="text-white text-6xl mb-2 font-dima-shekasteh">
              {headline}
            </h1>
            <div className="w-2/5 h-0 border-b border-t border-yellow-narges"></div>
          </div>

          <div dir="rtl" className="text-2xl font-lateef text-white">
            {description.map((d, i) => (
              <p key={`desc-${i}`}>{d}</p>
            ))}
            <p className="double-border-fancy mt-8">{kicker}</p>
          </div>

          <div className="mt-8">
            <div
              dir="rtl"
              className="flex flex-row text-white text-center font-lateef text-2xl justify-evenly mb-8"
            >
              <p>
                {date.day} {date.month}
                <br />
                {date.year}
              </p>
              <div className="border-r border-l border-yellow-narges"></div>
              <p>
                {date.weekday}
                <br />
                {time.start} - {time.end}
              </p>
            </div>

            <p dir="rtl" className="my-4 font-lateef text-white">
              <FaLocationDot scale={2} className="inline text-sm" />
              &nbsp;&nbsp;
              <span className="text-xl">{location.text}</span>
            </p>

            <div className="flex flex-row justify-evenly">
              <link
                rel="stylesheet"
                href="https://cdn.addevent.com/libs/atc/themes/fff-theme-6/theme.css"
                media="all"
              />
              <Script
                src="https://cdn.addevent.com/libs/atc/1.6.1/atc.min.js"
                defer
                async
              />
              <div
                title="Add to Calendar"
                className="addeventatc text-center text-xl m-1 py-3 px-7 bg-yellow-narges text-white hover:bg-blue-vida-darker hover:text-black active:bg-blue-vida-darker active:text-white focus:bg-blue-vida-darker focus:text-black font-lateef"
                data-styling="none"
                data-id="Yu18096372"
              >
                Add to Calendar
              </div>
              <Link
                href={location.url}
                target="_blank"
                className="addeventatc text-center text-xl m-1 py-3 px-7 bg-yellow-narges text-white hover:bg-blue-vida-darker hover:text-black active:bg-blue-vida-darker active:text-white focus:bg-blue-vida-darker focus:text-black font-lateef"
              >
                {location.btnText}
              </Link>
            </div>
          </div>
          <Image
            className="absolute top-auto left-auto right-auto bottom-auto opacity-5 -z-50"
            src={bgSrc}
            alt="bg"
          />
        </div>
      </section>
    </main>
  );
}
