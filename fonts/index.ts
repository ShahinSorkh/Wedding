import { Lateef } from "next/font/google";
import localFont from "next/font/local";

export const lateef = Lateef({
  subsets: ["latin", "arabic"],
  weight: ["200", "300", "400", "500", "600", "700", "800"],
  variable: "--font-lateef",
});
export const dimaShekasteh = localFont({
  src: "./Dima Shekasteh.ttf",
  variable: "--font-dima-shekasteh",
});
