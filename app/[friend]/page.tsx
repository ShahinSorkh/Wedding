import MainSection from "@/components/main-section";
import data from "../data.json";
import pages from "./data.json";

export const dynamic = "error";
export const dynamicParams = false;
export const metadata = { title: data.title };

export async function generateStaticParams() {
  return Object.keys(pages).map((friend) => ({
    friend,
  }));
}

type Props = { params: { friend: keyof typeof pages } };
export default function FreindsPage({ params }: Props) {
  return (
    <MainSection
      imageAlt={data.title}
      {...data}
      kicker={pages[params.friend]}
    />
  );
}
