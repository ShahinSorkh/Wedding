import {
  dimaShekasteh,
  lateef,
} from "@/fonts";
import "./globals.scss";

type Props = { children: React.ReactNode };
export default function RootLayout({ children }: Props) {
  return (
    <html
      className={[
        lateef.variable,
        dimaShekasteh.variable,
        "overflow-x-hidden",
      ].join(" ")}
    >
      <body className="bg-blue-shahin">{children}</body>
    </html>
  );
}
