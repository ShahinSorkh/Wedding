import MainSection from "@/components/main-section";
import data from "./data.json";

export const metadata = { title: data.title };

export default function IndexPage() {
  return <MainSection imageAlt={data.title} {...data} />;
}
