/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    fontFamily: {
      lateef: ['var(--font-lateef)'],
      'dima-shekasteh': ['var(--font-dima-shekasteh)'],
    },
    extend: {
      screens: { xs: '361px' },
      maxWidth: {
        '1/2': '50%',
        '1/3': '33%',
        '2/3': '66%',
      },
      colors: {
        'yellow-narges': '#f1b300',
        'blue-foo': '#def4ff',
        'blue-vida': '#c4d3de',
        'blue-vida-darker': '#89a7b3',
        'blue-shahin': '#3e4967',
      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
    },
  },
  plugins: [],
}
